


#ifndef LOGGER_H
#define LOGGER_H


/* TODO: Do I want? Sounds very bulky to use compile what is in the windows header, especially when nothing else is needed from it
#ifdef _WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif*/
#include <unistd.h> // Has sleep?
#include <time.h> // Has some useful time stuff
#include <stdio.h>


// NOTE: HOW TO USE
//       As Header Only
//          Simply include logger.h
//
//       As Library
//          Place before #include "logger.h": #ifndef FULL_LOGGING\n #define FULL_LOGGING\n #endif
//          Call CREATE_FULL_LOGGING(logLevel, logVerbosity) at start of logging code (e.g. start of main())
//          Call DESTROY_FULL_LOGGING() at end of logging code (e.g. end of main())
//








#ifdef FULL_LOGGING
char loggerLevelSave;
char loggerEventSave;
char loggerVerbositySave;
char loggerInitialized;
struct Logger logger;
char loggingOn; // Turning on/off only works locally (within same file) if used as a header only

// NOTE: To use full logging you must link the library and use CREATE_... and DESTROY_... macros ONCE
//          (at start of logging code, at end of logging code. e.g. in main())
#define CREATE_FULL_LOGGING(LEVEL, EVENT, VERBOSITY) \
                CreateLogger( &logger, loggerLevelSave = LEVEL, loggerEventSave = EVENT, loggerVerbositySave = VERBOSITY, 0 ); \
                LOGGING_ON();
#define SET_LOGGING_LEVEL(LEVEL) loggerLevelStore = logger->level; logger->level = LEVEL;
#define RESET_LOGGING_LEVEL() logger->level = loggerLevelStore;
#define SET_LOGGING_VERBOSITY(VERBOSITY) loggerVerbosityStore = logger->verbosity; logger->verbosity = VERBOSITY;
#define RESET_LOGGING_VERBOSITY() logger->verbosity = loggerVerbosityStore;
#define DESTROY_FULL_LOGGING() DestroyLogger( &logger ); LOGGING_OFF();
#else
#define CREATE_FULL_LOGGING()
#define DESTROY_FULL_LOGGING()
__attribute__ ((unused)) char loggerLevelSave;
__attribute__ ((unused)) char loggerEventSave;
__attribute__ ((unused)) char loggerVerbositySave;
__attribute__ ((unused)) char loggerInitialized;
__attribute__ ((unused)) struct Logger logger;
__attribute__ ((unused)) static char loggingOn = 1; // Turning on/off only works locally (w/in same file) if used as a header only
#endif


#define LOGGING_OFF() loggingOn = 0;
#define LOGGING_ON() loggingOn = 1;

// TODO: This #if/else may not work as intended when doing NO_LOGS since the actual logging is done outside of this conditional
#ifdef NO_LOGS
    #define TIME_STAMP 
    #define LOG_FATAL( ... ) 
    #define LOG_CRITICAL( ... ) 
    #define LOG_ERROR( ... ) 
    #define LOG_WARNING( ... ) 
    #define LOG_DEBUG( ... ) 
    #define LOG_ANNOYING( ... ) 
    #define LOG_INFO( ... ) 
    #define LOG_TRACE( ... ) 
    #define LOG_CHECKPOINT( ... ) 
    
    #define LOG_DATA( ... ) 
    #define LOG_STEP( step, ... ) 
#else
    #define LOG_WRAP_START if(loggingOn){ 
    #define LOG_WRAP_END }
#endif


// TODO: Move LOG_WRAP_START and END within LOG_WRAP_LEVEL_PRINT??? Otherwise lots of duplicate?

#ifdef FULL_LOGGING
    // TODO Consider how to use VERBOSITY in the future, for now I'm giving up on it (:
    #define LOG_WRAP_LEVEL_PRINT(LEVEL_STRING, LEVEL, VERBOSITY, ...) \
                                        {char argBuffer[256]={0}; sprintf(argBuffer, __VA_ARGS__); \
                                        switch(LEVEL) {\
                                            case llANNOYING: { LogAnnoying( &logger, argBuffer, (int) VERBOSITY ); } break; \
                                            case llDEBUG: { LogDebug( &logger, argBuffer, (int) VERBOSITY ); } break; \
                                            case llINFO: { LogInfo( &logger, argBuffer, (int) VERBOSITY ); } break; \
                                            case llWARN: { LogWarn( &logger, argBuffer, (int) VERBOSITY ); } break; \
                                            case llERROR: { LogError( &logger, argBuffer, (int) VERBOSITY ); } break; \
                                            case llCRITICAL: { LogCritical( &logger, argBuffer, (int) VERBOSITY ); } break; \
                                            case llFATAL: { LogFatal( &logger, argBuffer, (int) VERBOSITY ); } break; \
                                            default: break; }}
    #define LOG_WRAP_EVENT_PRINT(EVENT_PREFIX, EVENT, ...) \
                                        {char argBuffer[256]={0}; sprintf(argBuffer, __VA_ARGS__); \
                                        switch(EVENT) {\
                                            case leTRACE: { LogTrace( &logger, EVENT_PREFIX, argBuffer ); } break; \
                                            case leCHECKPOINT: { LogCheckpoint( &logger, EVENT_PREFIX, argBuffer ); } break; \
                                            case leMILESTONE: { LogMilestone( &logger, EVENT_PREFIX, argBuffer ); } break; \
                                            case leSTEP: { LogStep( &logger, EVENT_PREFIX, argBuffer ); } break; \
                                            case leDATA: { LogData( &logger, EVENT_PREFIX, argBuffer ); } break; \
                                            default: break; }}
#else
    #define LOG_WRAP_LEVEL_PRINT(LEVEL_STRING, LEVEL, ...) \
            STANDARD_LOG_HEADER; printf(LEVEL_STRING); printf(": "); printf( __VA_ARGS__ ); STANDARD_LOG_FOOTER;
    #define LOG_WRAP_EVENT_PRINT(EVENT_PREFIX, EVENT, ...) \
            EVENT_LOG_HEADER; printf(EVENT_PREFIX); printf(": "); printf( __VA_ARGS__ ); EVENT_LOG_FOOTER;
#endif


#ifdef FULL_LOGGING
    #define LOG_ANNOYING( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("ANNOYING", llANNOYING, llvDEFAULT, __VA_ARGS__); LOG_WRAP_END
    #define LOG_DEBUG( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("DEBUG", llDEBUG, llvDEFAULT, __VA_ARGS__); LOG_WRAP_END
    #define LOG_INFO( ... )  LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("INFO", llINFO, llvDEFAULT, __VA_ARGS__); LOG_WRAP_END
    #define LOG_WARNING( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("WARNING", llWARN, llvDEFAULT, __VA_ARGS__); LOG_WRAP_END
    #define LOG_ERROR( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("\tERROR", llERROR, llvDEFAULT, __VA_ARGS__); LOG_WRAP_END // \t for clarity, in future use color
    #define LOG_CRITICAL( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("CRITICAL", llCRITICAL, llvDEFAULT, __VA_ARGS__); LOG_WRAP_END
    #define LOG_FATAL( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("FATAL", llFATAL, llvDEFAULT, __VA_ARGS__); LOG_WRAP_END

    #define IF_DEBUG( ... ) if( logger.level & llDEBUG ) { __VA_ARGS__ }
#else
    #define LOG_ANNOYING( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("ANNOYING", llANNOYING, __VA_ARGS__); LOG_WRAP_END
    #define LOG_DEBUG( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("DEBUG", llDEBUG, __VA_ARGS__); LOG_WRAP_END
    #define LOG_INFO( ... )  LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("INFO", llINFO, __VA_ARGS__); LOG_WRAP_END
    #define LOG_WARNING( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("WARNING", llWARN, __VA_ARGS__); LOG_WRAP_END
    #define LOG_ERROR( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("\tERROR", llERROR, __VA_ARGS__); LOG_WRAP_END // \t for clarity, in future use color
    #define LOG_CRITICAL( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("CRITICAL", llCRITICAL, __VA_ARGS__); LOG_WRAP_END
    #define LOG_FATAL( ... ) LOG_WRAP_START LOG_WRAP_LEVEL_PRINT("FATAL", llFATAL, __VA_ARGS__); LOG_WRAP_END

    #define IF_DEBUG( ... ) __VA_ARGS__ // Defaulting to always executing
#endif



    #define LOG_TRACE( ... ) LOG_WRAP_START LOG_WRAP_EVENT_PRINT("TRACE", leTRACE, __VA_ARGS__); LOG_WRAP_END // TODO: Info of where at and vars?
    // TODO: Impt thing completed? Print results?
    #define LOG_CHECKPOINT( ... ) LOG_WRAP_START LOG_WRAP_EVENT_PRINT("CHECKPOINT", leCHECKPOINT,__VA_ARGS__); LOG_WRAP_END CheckError();
    #define LOG_DATA( ... )       LOG_WRAP_START LOG_WRAP_EVENT_PRINT("DATA", leDATA, __VA_ARGS__); LOG_WRAP_END // TODO: Variable?

    // TODO: Something bigger than a checkpoint (which is start/end of function) -> bigger = End of lots of functions (e.g. engine initialized)
    #define LOG_MILESTONE( ... ) // Perhaps just make "MARK_MILESTONE" and have line spacing with dashes or something?
    
    //#define LOG_STEP( step, ... ) LOG_WRAP_START printf("STEP #%i: ",step); printf(__VA_ARGS__); LOG_WRAP_END // TODO: Number?
    #define LOG_STEP( step, ... ) LOG_WRAP_START {char eventStringBuffer[256]={0}; sprintf(eventStringBuffer, "Step #%i", step); \
                                  LOG_WRAP_EVENT_PRINT(eventStringBuffer, leSTEP, __VA_ARGS__)} LOG_WRAP_END



#define SEPARATE printf("------------------------------------------\n"); // TODO: Console width (platform dependent)? Parameter?
#define BAR printf("|"); 

#define LINE_BREAK(count) for( int i = 0; i < count; ++i ) printf("\n");

#define STANDARD_LOG_HEADER TIME_STAMP; BAR; FILE_STAMP; BAR; LINE_STAMP; BAR;
#define STANDARD_LOG_FOOTER printf("\n");

#define EVENT_LOG_HEADER TIME_STAMP; BAR; FILE_STAMP; BAR; LINE_STAMP; BAR;
#define EVENT_LOG_FOOTER printf("\n");

#define TIME_STAMP // TODO
#define FILE_STAMP printf("%s", __FILE__);
#define LINE_STAMP printf("%i", __LINE__);

#define PAUSE // TODO: scanf? but scanf bad (uesr can enter in any # of characters and cause buffer overflow, fgets better)
#define SLEEP( seconds ) sleep( seconds )

#define TIME_START
#define TIME_END

#define MEMORY_DUMP



// TODO: This will probably be something defined in client's code (e.g. in a common file that uses my logging)
void glCheckError_(const char* file, int line);
#define glCheckError() glCheckError_(__FILE__, __LINE__)

#ifdef OPENGL
    #define CheckError() glCheckError()
#else
    #define CheckError() 
#endif




enum LoggerLevels
{
    // Bit fields (GCC needed) to allow for flexible options for logging levels
    // E.g. DEBUG | ERROR -> Sets these two logging levels and a bit field of 001001
    llOFF = 0b00000000,
    llANNOYING = 0b00000001, // TODO what numbers should these be? Renumber these...
    llDEBUG = 0b00000010, // TODO what numbers should these be? Renumber these...
    llINFO = 0b00000100,
    llWARN = 0b00001000,
    llERROR = 0b00010000,
    llCRITICAL = 0b00100000,
    llFATAL = 0b01000000,

    llPRESET_DESPERATE = llANNOYING | llDEBUG | llINFO | llWARN | llERROR | llCRITICAL | llFATAL,
    llPRESET_PARANOID = llDEBUG | llINFO | llWARN | llERROR | llCRITICAL | llFATAL,
    llPRESET_CURIOUS = llINFO | llWARN | llERROR | llCRITICAL | llFATAL,
    llPRESET_CAUTIOUS = llWARN | llERROR | llCRITICAL | llFATAL,
    llPRESET_CONFIDENT = llERROR | llCRITICAL | llFATAL,
    llPRESET_NAIVE = llCRITICAL | llFATAL,
    llPRESET_IGNORANT = llFATAL,
    llPRESET_BLIND = llOFF
};

enum LoggerEvents
{
    leOFF = 0b00000000,
    leTRACE = 0b00000001,
    leCHECKPOINT = 0b00000010,
    leMILESTONE = 0b00000100,
    leSTEP = 0b00001000,
    leDATA = 0b00010000,

    lePRESET_VERY_VERY_GRANULAR = leTRACE | leCHECKPOINT | leMILESTONE | leSTEP | leDATA,
    lePRESET_VERY_GRANULAR = leCHECKPOINT | leMILESTONE | leSTEP | leDATA,
    lePRESET_GRANULAR = leMILESTONE | leSTEP | leDATA,
};

enum LoggerLevelVerbosity
{
    llvNOT_VERBOSE = 0b0000000,
    llvVERBOSE = 0b0000001,

    // TODO: Below currently not supported since using bit fields in conditional, can only support binary options
    llvVERY_VERBOSE = 0b0000010,
    llvVERY_VERY_VERBOSE = 0b0000011,

    llvDEFAULT = llvNOT_VERBOSE, // Default for macro expansion (case when no verbosity full logging functionality)

    llvMAX = 0b11111111 // Max possible value for type checking vs a pointer type (workaround for #defines above)
};

// An extra level of granularity for log levels (verbose or not verbose?)
enum LoggerVerbosity
{
    lvNONE = 0b00000000,
    lvANNOYING = 0b00000001,
    lvDEBUG = 0b00000010,
    lvINFO = 0b00000100,
    lvWARN = 0b00001000,
    lvERROR = 0b00010000,
    lvCRITICAL = 0b00100000,
    lvFATAL = 0b01000000,

    lvPRESET_ALL = 0b11111111,
    lvPRESET_NONE = 0b00000000,

    lvPRESET_FINE_POINTS = lvDEBUG | lvINFO,
    lvPRESET_IMPORTANT = lvWARN | lvERROR | lvCRITICAL | lvFATAL
};

enum LoggerTypes
{
    LOGGER_TYPE_DATA,
    LOGGER_TYPE_STEP,
    LOGGER_TYPE_TRACE,
    LOGGER_TYPE_CHECKPOINT
};

struct Logger // TODO: Design
{
    char level;
    char event;
    char verbosity; 

    char** criticals;
    char** errors;
    char** warnings;

    int numCriticals;
    int numErrors;
    int numWarnings;

    unsigned char storeLogsFlag: 1; // Flag, use 1 bit only TODO: Option of which logs to store? Bitfield?
};


void CreateLogger( struct Logger* loggerOut, char level, char event, char verbosity, unsigned char storeLogsFlag );

// -------------------- Standard Logging -------------------- 
void LogAnnoying( struct Logger* logger, char* message, char verbosity );
void LogDebug( struct Logger* logger, char* message, char verbosity );
void LogInfo( struct Logger* logger, char* message, char verbosity );
void LogWarn( struct Logger* logger, char* message, char verbosity );
void LogError( struct Logger* logger, char* message, char verbosity );
void LogCritical( struct Logger* logger, char* message, char verbosity );
void LogFatal( struct Logger* logger, char* message, char verbosity );

// -------------------- Event-Based Logging -------------------- 
void LogTrace( struct Logger* logger, char* prefix, char* message );
void LogCheckpoint( struct Logger* logger, char* prefix, char* message );
void LogMilestone( struct Logger* logger, char* prefix, char* message );
void LogStep( struct Logger* logger, char* prefix, char* message );
void LogData( struct Logger* logger, char* prefix, char* message );

void PrintLogs( struct Logger* logger ); // Print all errors currently in the logger
void FlushLogs( struct Logger* logger );

void DestroyLogger( struct Logger* logger );

                                        /*void foo(int LEVEL,int VERBOSITY, char argBuffer[256]) {
                                        switch(LEVEL) {\
                                            case llDEBUG: { LogDebug( &logger, argBuffer, VERBOSITY ); } break; \
                                            case llINFO: { LogInfo( &logger, argBuffer, VERBOSITY ); } break; \
                                            case llWARN: { LogWarn( &logger, argBuffer, VERBOSITY ); } break; \
                                            case llERROR: { LogError( &logger, argBuffer, VERBOSITY ); } break; \
                                            case llCRITICAL: { LogCritical( &logger, argBuffer, VERBOSITY ); } break; \
                                            case llFATAL: { LogFatal( &logger, argBuffer, VERBOSITY ); } break; \
                                            default: break; }}*/

#endif // LOGGER_H






#include "logTest1.h"

#define FULL_LOGGING
#include "logger.h"


struct test { int x; };

void printTest()
{
    LINE_BREAK(2);

    /*LOG_INFO(llvVERBOSE, "Verbose Info!");
    LOG_INFO(llvNOT_VERBOSE, "Not Verbose Info!");

    LOG_DEBUG(llvVERBOSE, "Verbose Debug!");
    LOG_DEBUG(llvNOT_VERBOSE, "Not Verbose Debug!");

    LOG_WARNING(llvVERBOSE, "Verbose Warn!");
    LOG_WARNING(llvNOT_VERBOSE, "Not Verbose Warn!");

    LOG_ERROR(llvVERBOSE, "Verbose Error!");
    LOG_ERROR(llvNOT_VERBOSE, "Not Verbose Error!");

    LOG_CRITICAL(llvVERBOSE, "Verbose Critical!");
    LOG_CRITICAL(llvNOT_VERBOSE, "Not Verbose Critical!");

    LOG_FATAL(llvVERBOSE, "Verbose FATAL!");
    LOG_FATAL(llvNOT_VERBOSE, "Not Verbose Fatal!");
    LINE_BREAK(1);
    LOG_DEBUG_TEST(llvVERBOSE, "SPECIAL TESTING");
    LOG_DEBUG_TEST("SPECIAL TESTING");*/

    LOG_INFO("--------------ClientInfo--------------");
    struct test testStruct; testStruct.x = 2;
    LOG_INFO("Name: %i, %i", testStruct.x, 10);

    LOG_INFO("Verbose Info!");
    LOG_INFO("Verbose Info! %s", "MORE");
    //LOG_INFO(llvVERBOSE, "Verbose Info! %s", "MORE");
    //LOG_INFO(llvNOT_VERBOSE, "Verbose Info!");
    LOG_ANNOYING("Verbose Annoying!");
    LOG_DEBUG("Verbose Debug!");
    LOG_WARNING("Verbose Warn!");
    LOG_ERROR("Verbose Error!");
    LOG_CRITICAL("Verbose Critical!");
    LOG_FATAL("Verbose FATAL!");

    IF_DEBUG(LOG_INFO("YAYY IF DEBUG"););

    LINE_BREAK(1);

    LOG_TRACE("Trace!");
    LOG_CHECKPOINT("printTest");
    LOG_MILESTONE("Milestone!");
    //LOG_DATA(...);

    LOG_STEP(1, "STEP1");
    LOG_STEP(2, "STEP2");
    LOG_STEP(3, "STEP3");
    LOG_STEP(4, "STEP4");

    LINE_BREAK(2);
}



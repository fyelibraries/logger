
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

/*#include "common.h"
 
#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h>
#include <GL/gl.h>*/

#include "logger.h"

/*#ifdef FULL_LOGGING
char loggingOn = 1;
static Logger logger;
#endif*/

#ifdef FULL_LOGGING
extern char loggerLevelSave;
extern char loggerEventSave;
extern char loggerVerbositySave;
extern char loggerInitialized = 0; // Used in case CreateLogger is called multiple times (e.g. from the Macro)
extern struct Logger logger;
extern char loggingOn = 1; // Currently requires logger.c to be in translation unit (i.e. compiled with what includes this)
                       //  Meaning if this logging functionality is only used as a #include then global logs on/off won't work
                       //  This #conditional allows the use of the logging functionality as a header only without breaking anything
                       //  LOGGING_ON/OFF can then only be used w/in the same file (i.e. not across files where both include this)
#define INIT_FULL_LOGGING() //CreateLogger( &logger, 0 );
#define CLEAN_FULL_LOGGING() //DestroyLogger( &logger );
#endif


// TODO: How handle these special case logging? Callback? Or leave to a common file in FYEngine to implement this?
/*void glCheckError_(const char* file, int line)
{
    // If OpenGL is not the current context, then stop immediately
#ifdef GLFW
    if( !glfwGetCurrentContext() )
        return;
#endif // Expand if statement when we create OpenGL context in other ways

    GLenum error_code;
    while( (error_code = glGetError()) != GL_NO_ERROR )
    {
        char* error;

        switch( error_code )
        {
            case GL_INVALID_ENUM: error = (char*) "GL_INVALID_ENUM"; break;
            case GL_INVALID_VALUE: error = (char*) "GL_INVALID_VALUE"; break;
            case GL_INVALID_OPERATION: error = (char*) "GL_INVALID_OPERATION"; break;
            case GL_STACK_OVERFLOW: error = (char*) "GL_STACK_OVERFLOW"; break;
            case GL_STACK_UNDERFLOW: error = (char*) "GL_STACK_UNDERFLOW"; break;
            case GL_OUT_OF_MEMORY: error = (char*) "GL_OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: error = (char*) "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
        }
        LOG_ERROR("%s | %s (%i)", error, file, line);
    }
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)*/



// -------------------- Macro Function Helpers -------------------- 

// TODO: Below verbose loging is on hold for later (:
void PrintVerboseLog( struct Logger* logger, enum LoggerLevels level, 
                      enum LoggerLevelVerbosity verbosity, char* message )
{
    switch(level) 
    {
        case llDEBUG: { LogDebug( logger, message, verbosity ); } break; 
        case llINFO: { LogInfo( logger, message, verbosity ); } break; 
        case llWARN: { LogWarn( logger, message, verbosity ); } break; 
        case llERROR: { LogError( logger, message, verbosity ); } break; 
        case llCRITICAL: { LogCritical( logger, message, verbosity ); } break; 
        case llFATAL: { LogFatal( logger, message, verbosity ); } break; 
        default: break;
    }
}

/*void PrintEventLog( struct Logger* logger, enum LoggerEvents event, 
                    enum LoggerLevelVerbosity verbosity, const char* message )
{
    switch(event) 
    {
        case leTRACE: { LogTrace( &logger, EVENT_PREFIX, argBuffer ); } break; 
        case leCHECKPOINT: { LogCheckpoint( &logger, EVENT_PREFIX, argBuffer ); } break; 
        case leMILESTONE: { LogMilestone( &logger, EVENT_PREFIX, argBuffer ); } break;
        case leSTEP: { LogStep( &logger, EVENT_PREFIX, argBuffer ); } break; 
        case leDATA: { LogData( &logger, EVENT_PREFIX, argBuffer ); } break;
        default: break;
    }
}*/

void CreateLogger( struct Logger* loggerOut, char level, char event, char verbosity, unsigned char storeLogsFlag )
{
    loggerOut->level = level;
    loggerOut->event = event;
    loggerOut->verbosity = verbosity;

    if( loggerInitialized ) free( loggerOut->criticals );
    loggerOut->criticals = 0;
    loggerOut->numCriticals = 0;

    if( loggerInitialized ) free( loggerOut->errors );
    loggerOut->errors = 0;
    loggerOut->numErrors = 0;

    if( loggerInitialized ) free( loggerOut->warnings );
    loggerOut->warnings = 0;
    loggerOut->numWarnings = 0;

    loggerOut->storeLogsFlag = storeLogsFlag;

    loggerInitialized = 1;
}


// -------------------- Standard Logging -------------------- 
void LogAnnoying( struct Logger* logger, char* message, char verbosity )
{
    if( logger->level & llANNOYING && (verbosity == llvNOT_VERBOSE || logger->verbosity & lvANNOYING) )
    {
        STANDARD_LOG_HEADER;
        printf("ANNOYING: %s", message);
        STANDARD_LOG_FOOTER;
    }
}

void LogDebug( struct Logger* logger, char* message, char verbosity )
{
    if( logger->level & llDEBUG && (verbosity == llvNOT_VERBOSE || logger->verbosity & lvDEBUG) )
    {
        STANDARD_LOG_HEADER;
        printf("DEBUG: %s", message);
        STANDARD_LOG_FOOTER;
    }
}

void LogInfo( struct Logger* logger, char* message, char verbosity )
{
    if( logger->level & llINFO  && (verbosity == llvNOT_VERBOSE || logger->verbosity & lvINFO) )
    {
        STANDARD_LOG_HEADER;
        printf("INFO: %s", message);
        STANDARD_LOG_FOOTER;
    }
}

void LogWarn( struct Logger* logger, char* message, char verbosity )
{
    if( logger->level & llWARN  && (verbosity == llvNOT_VERBOSE || logger->verbosity & lvWARN) )
    {
        STANDARD_LOG_HEADER;
        printf("WARN: %s", message);
        STANDARD_LOG_FOOTER;
    }
}

void LogError( struct Logger* logger, char* message, char verbosity )
{
    if( logger->level & llERROR  && (verbosity == llvNOT_VERBOSE || logger->verbosity & lvERROR) )
    {
        STANDARD_LOG_HEADER;
        printf("\tERROR: %s", message);
        STANDARD_LOG_FOOTER;
    }
}

void LogCritical( struct Logger* logger, char* message, char verbosity )
{
    if( logger->level & llCRITICAL  && (verbosity == llvNOT_VERBOSE || logger->verbosity & lvCRITICAL) )
    {
        STANDARD_LOG_HEADER;
        printf("\tCRITICAL: %s", message);
        STANDARD_LOG_FOOTER;
    }
}

void LogFatal( struct Logger* logger, char* message, char verbosity )
{
    if( logger->level & llFATAL  && (verbosity == llvNOT_VERBOSE || logger->verbosity & lvFATAL) )
    {
        STANDARD_LOG_HEADER;
        printf("\tFATAL: %s", message);
        STANDARD_LOG_FOOTER;
    }
}

// -------------------- Event-Based Logging -------------------- 
void LogTrace( struct Logger* logger, char* prefix, char* message )
{
    EVENT_LOG_HEADER;
    printf("%s: %s", prefix, message);
    EVENT_LOG_FOOTER;
}

void LogCheckpoint( struct Logger* logger, char* prefix, char* message )
{
    if( logger->event & leCHECKPOINT )
    {
        printf("\n");
        EVENT_LOG_HEADER;
        printf("%s: %s", prefix, message);
        EVENT_LOG_FOOTER;
    }
}

void LogMilestone( struct Logger* logger, char* prefix, char* message )
{
    EVENT_LOG_HEADER;
    printf("%s: %s", prefix, message);
    EVENT_LOG_FOOTER;
}

void LogStep( struct Logger* logger, char* prefix, char* message )
{
    EVENT_LOG_HEADER;
    printf("%s: %s", prefix, message);
    EVENT_LOG_FOOTER;
}

void LogData( struct Logger* logger, char* prefix, char* message )
{
    EVENT_LOG_HEADER;
    printf("%s: %s", prefix, message);
    EVENT_LOG_FOOTER;
}


// -------------------- Log Manipulation -------------------- 
void PrintLogs( struct Logger* logger )
{
    SEPARATE;
    for( int i = 0; i < logger->numCriticals; i++ )
    {
        LOG_CRITICAL("%s", logger->criticals[i]);
    }

    SEPARATE;
    for( int i = 0; i < logger->numErrors; i++ )
    {
        LOG_ERROR("%s", logger->errors[i]);
    }

    SEPARATE;
    for( int i = 0; i < logger->numWarnings; i++ )
    {
        LOG_WARNING("%s", logger->warnings[i]);
    }
}

void FlushLogs( struct Logger* logger )
{
    if( logger->storeLogsFlag )
    {
        for( int i = 0; i < logger->numCriticals; i++ )
        {
            free( logger->criticals[i] );
        }
        free( logger->criticals );

        for( int i = 0; i < logger->numErrors; i++ )
        {
            free( logger->errors[i] );
        }
        free( logger->errors );

        for( int i = 0; i < logger->numWarnings; i++ )
        {
            free( logger->warnings[i] );
        }
        free( logger->warnings );
    }
}

void DestroyLogger( struct Logger* logger )
{
    FlushLogs( logger );

}






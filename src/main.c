

#include "logTest1.h"

#define FULL_LOGGING
#include "logger.h"


int main( int argc, char** argv )
{
    CREATE_FULL_LOGGING(llPRESET_DESPERATE, lePRESET_VERY_VERY_GRANULAR, lvPRESET_FINE_POINTS);
    //CREATE_FULL_LOGGING(llPRESET_CAUTIOUS, lvPRESET_ALL);
    //CREATE_FULL_LOGGING(llPRESET_CONFIDENT, lvPRESET_ALL);

    //LOGGING_OFF();

    LOG_CHECKPOINT("main");

    //LOGGING_ON();

    printTest();

    DESTROY_FULL_LOGGING();
    return 0;
}



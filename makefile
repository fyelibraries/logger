
INC= inc
SRC= src
OBJ= obj
LIB= lib

MK_LIB = ar rcs
LIB_NAME = logger.a

MAIN=main.c


_LIBRARY_DEPS= logger.h
# TODO: WHY HAVE MAIN in LIBRARY?  And is library deps necesary?
LIBRARY_SOURCES= $(SRC)/$(MAIN) $(patsubst %.h, $(SRC)/%.c, $(_LIBRARY_DEPS))
LIBRARY_OBJECTS= $(OBJ)/$(subst .c,.o,$(MAIN)) $(patsubst %.h, $(OBJ)/%.o, $(_LIBRARY_DEPS))

_DEPS= $(_LIBRARY_DEPS) logTest1.h
HEADERS= $(patsubst %.h, $(INC)/%.h, $(_DEPS))

SOURCES= $(SRC)/$(MAIN) $(patsubst %.h, $(SRC)/%.c, $(_DEPS))
OBJECTS= $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SOURCES))


INCLUDES= -I$(INC)
LIBRARIES= 

CFLAGS= -Wall $(INCLUDES) $(LIBRARIES)

CC= gcc



run: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@

$(OBJ)/%.o: $(SRC)/%.c $(HEADERS)
	if not exist "$(OBJ)" mkdir $(OBJ)
	$(CC) $(CFLAGS) -c $< -o $@


library: $(LIBRARY_OBJECTS)
	if not exist $(LIB) mkdir $(LIB)
	$(MK_LIB) $(LIB)/lib$(LIB_NAME) $(LIBRARY_OBJECTS)



clean:
ifndef OBJ
	$(error OBJ is not set)
else
	del /q $(OBJ)\*
endif

library_clean:
ifndef LIB
	$(error LIB is not set)
else
	del /q $(LIB)\*
endif

